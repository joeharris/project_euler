#!/usr/bin/python
def pentNum(n):
	return n*(3*n-1)/2

def isPentNum(num):
	return (num in pentNums)
pentNums = []
# populate list of pent nums
for i in range(1,2000):
	pentNums.append(pentNum(i))

# loop through permutations
for i in range(len(pentNums)):
	for j in range(i, len(pentNums)):
		#print str(i) + "," + str(j)
		tmpSum = pentNums[i] + pentNums[j]
		tmpDiff = abs(pentNums[i] - pentNums[j])
		if (isPentNum(tmpSum) and isPentNum(tmpDiff)):
			print "i: " + str(i) + ", j: " + str(j) + ".. diff: " + str(tmpDiff)