#!/usr/bin/python
import math
def toBin(num):
	binary = ""
	while (num > 0):
		binary += str(int(num % 2))
		num = math.floor(int(num/2))
	return binary[::-1]

def isPalindrome(num):
	num = str(num)
	for i in range(0, int(math.floor(len(num)/2+1))):
		if (num[i] != num[len(num)-i-1]):
			return False
	return True
total = 0
for i in range(1,1000001):
	if (isPalindrome(i) and isPalindrome(toBin(i))):
		total += i
		print i
		print total
print "END"