#!/usr/bin/python
#600851475143

def isPrime(number):
	for i in range(2,int(number**(0.5))+1):
		if (number % i == 0):
			return False
	return True

def findPrimeFactors(number):
	factors = []
	for i in range(2,int(number**(0.5))+1):
		if (number % i == 0):
			if i not in factors and isPrime(i):
				factors.append(i)
			if number/i not in factors and isPrime(number/i):
				factors.append(number/i)
	return factors

print max(findPrimeFactors(600851475143))