#!/usr/bin/python
def collatzLength(num):
	length = 1
	if (num == 1):
		return 1
	elif (num % 2 == 0):
		length += collatzLength(num/2)
		return length
	elif (num % 2 != 0):
		length += collatzLength(3*num+1)
		return length
	return length
# for each number work out collatz chain length
# lengths = []
# for i in range(1,1000000):
# 	lengths.append(collatzLength(i))
# 	#print collatzLength(i)
# print lengths.index(max(lengths))+1
# print "length: "+str(collatzLength(lengths.index(max(lengths))+1))
print collatzLength(837799)