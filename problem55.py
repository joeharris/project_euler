#!/usr/bin/python
import math
def isPalindrome(num):
	num = str(num)
	for i in range(0, int(math.floor(len(num)/2+1))):
		if (num[i] != num[len(num)-i-1]):
			return False
	return True

def lychrel(num):
	i = 0
	while(i < 50):	
		reverseNum = str(num)[::-1]	
		if (isPalindrome(int(reverseNum) + int(num))):
			return False
		else:
			num = int(reverseNum) + int(num)
			i += 1
	return True
total = 0
for i in range(0,10000):
	print(i)
	if (lychrel(i)):
		print("True")
		total += 1
print(total)
