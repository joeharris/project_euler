#!/usr/bin/python
def factorial(num):
	total = num
	if (num == 1 or num == 0):
		return 1
	total *= factorial(num-1)
	return total

def testNum(num):
	total = 0
	for i in range(0, len(str(num))):
		total+=factorial(int(str(num)[i]))
	if (total == num):
		return True
	else:
		return False
total = 0
num = 10
while True:
	if (testNum(num)):
		total+= num
		print total
	if (num > 100000000):
		break
	num += 1
