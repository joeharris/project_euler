#!/usr/bin/python
def isSumOfDigitsFifthPowers(num):
	total = 0
	for i in range(0,len(str(num))):
		total += int(str(num)[i])**5
	if (total == num):
		return True
	else:
		return False
i = 2
total = 0
while True:
	if (isSumOfDigitsFifthPowers(i)):
		total += i
		print "i: " + str(i)
		print total
	if (i > 1000000000):
		break
	i += 1