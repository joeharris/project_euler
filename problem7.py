#!/usr/bin/python
def isPrime(number):
	for i in range(2,int(number**(0.5))+1):
		if (number % i == 0):
			return False
	return True

j = 1
primes = []
while (len(primes) <= 10001):
	if (isPrime(j)):
		primes.append(j)
	j+=1
print primes[-1]