#!/usr/bin/python
import itertools
def isPrime(number):
	for i in range(2,int(number**(0.5))+1):
		if (number % i == 0):
			return False
	return True

def getPandigital(n):
	num = ""
	for i in range(1,n+1):
		num += str(i)
	pans = list(itertools.permutations(num))
	return pans
primes = []

for digits in getPandigital(7):
	num = ''.join(digits)
	if (isPrime(int(num))):
		primes.append(num)
print (max(primes))