#!/usr/bin/python
def sumDigital(num):
	total = 0;
	for i in range(0, len(str(num))):
		total += int(str(num)[i]);
	return total;
	
currentMax = 0;
for a in range(1,101):
	for b in range(1,101):
		num = a**b;
		total = sumDigital(num);
		if(total > currentMax):
			currentMax = total;
print(currentMax);
		