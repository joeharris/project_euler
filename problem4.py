#!/usr/bin/python
# 998001 is largest number made of product of
# two 3-digit numbers
import math
def isPalindrome(num):
	num = str(num)
	for i in range(0, int(math.floor(len(num)/2+1))):
		if (num[i] != num[len(num)-i-1]):
			return False
	return True
for i in range(10000,998001):
	result = isPalindrome(i)
	if (result):
		for j in range(100,1000):
			if (len(str(int(i/j))) == 3 and i % j == 0):
				print i
				break