#!/usr/bin/python
import string
import re
def triangle(n):
	return 0.5*n*(n+1)

def getLetterValue(word):
	total = 0
	for i in range(0,len(str(word))):
		total += ord(word[i].lower()) - 96
	return total

triNums = []
count = 0
for i in range(0, 100):
	triNums.append(triangle(i))
f = open("words.txt","r")
wordF = f.read()
f.close()
words = wordF.split(",")
for word in words:
 	if(int(getLetterValue(re.sub('"','',word))) in triNums):
 		count += 1
print count
