#!/usr/bin/python
f = open("number","r");
number = f.read();
currentMax = 0;
currentProd = 1;
lengthSum = 13;
for pos in range(0,len(str(number))):
	currentProd = 1;
	for sumPos in range(0,lengthSum):
		if (pos + sumPos < len(number)-1): 
			currentProd *= int(str(number)[pos+sumPos]);
	if (currentProd > currentMax):
		currentMax = currentProd;
print(currentMax);